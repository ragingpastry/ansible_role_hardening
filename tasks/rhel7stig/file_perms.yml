---
# Copyright 2016, Rackspace US, Inc.
# Copyright 2019, Global InfoTek, Inc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

- name: Reset RPM Permissions
  block:
  - name: "Read list of files with incorrect permissions"
    shell: "rpm -Va | grep '^.M' | cut -d ' ' -f5- | sed -r 's;^.*\\s+(.+);\\1;g'"
    register: files_with_incorrect_permissions
    failed_when: False
    changed_when: False
    check_mode: no

  - name: "Correct file permissions with RPM"
    shell: "rpm --quiet --setperms $(rpm -qf '{{ item }}')"
    with_items: "{{ files_with_incorrect_permissions.stdout_lines }}"
    when:
      - files_with_incorrect_permissions.stdout_lines | length > 0

  - name: "Correct file permissions with RPM"
    shell: "rpm --quiet --setugids $(rpm -qf '{{ item }}')"
    with_items: "{{ files_with_incorrect_permissions.stdout_lines }}"
  when:
    - security_reset_perm_ownership | bool
  tags:
    - rpm_verify_permissions
    - high_severity
    - restrict_strategy
    - high_complexity
    - medium_disruption
    - NIST-800-53-AC-6
    - NIST-800-53-AU-9(1)
    - NIST-800-53-AU-9(3)
    - NIST-800-53-CM-6(d)
    - NIST-800-53-CM-6(3)
    - NIST-800-171-3.3.8
    - NIST-800-171-3.4.1
    - PCI-DSS-Req-11.5
    - CJIS-5.10.4.1
    - DISA-STIG-RHEL-07-010010

- name: V-71849 - Reset file permissions/ownership to vendor values
  shell: "rpm {{ item[0] }} `rpm -qf {{ item[1] }}`"
  args:
    warn: no
  changed_when: false
  with_nested:
    - ['--setperms', '--setugids']
    - "{{ rpmverify_package_list.stdout_lines | default([]) }}"
  when:
    - not check_mode | bool
    - ansible_pkg_mgr in ['yum', 'zypper']
    - rpmverify_package_list is defined
    - rpmverify_package_list.stdout_lines | length > 0
  async: 300
  poll: 0
  tags:
    - file_perms
    - high
    - V-71849
    # don't trigger ANSIBLE0013
    - skip_ansible_lint

- name: Set proper owner, group owner, and permissions on home directories
  file:
    dest: "{{ item.dir }}"
    owner: "{{ item.name }}"
    group: "{{ item.group.name }}"
    mode: "g-ws,o-rwxt"
  when:
    - item.uid >= 1000
    - item.name != 'nobody'
    - security_set_home_directory_permissions_and_owners | bool
  with_items: "{{ user_list.users | selectattr('uid', 'greaterthan', 999) | list }}"
  tags:
    - medium
    - file_perms
    - V-72017
    - V-72019
    - V-72021

- name: Check if /etc/cron.allow exists
  stat:
    path: /etc/cron.allow
  register: cron_allow_check
  tags:
    - always

- name: Set owner/group owner on /etc/cron.allow
  file:
    path: /etc/cron.allow
    owner: root
    group: root
  when:
    - cron_allow_check is defined
    - cron_allow_check.stat.exists
  tags:
    - medium
    - file_perms
    - V-72053
    - V-72055
